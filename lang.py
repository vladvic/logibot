from models.User import User, LangSetting
from models.Database import engine, session
from messages import *
from i18n import *

_LANG = 'RU'
LANG = {}
_tr = lambda what: _trans(what, _LANG)
_lang = 'RU'

def set_lang(userid, lang, canceled):
    global LANG
    userid = userid
    setting = session.query(LangSetting).filter(LangSetting.user_id==userid).one_or_none()

    if setting is None:
        setting = LangSetting()
        setting.user_id = userid
        session.add(setting)
        session.commit()

    setting.lang = lang
    session.commit()

    LANG[userid] = lang


def _trans(what, lng):
    loc = {}
    translation = None
    try:
        exec("import i18n.%s\ntranslation = i18n.%s.TRANSLATION" % (lng, lng), globals(), loc)
        translation = loc['translation']
    except:
        pass

    if translation is not None:
        return translation.get(what, what)

    return what


def get_translator(lang):
    return lambda x: _trans(x, lang)


def get_lang(userid):
    global LANG
    if userid not in LANG:
        LANG[userid] = None
        settings = session.query(LangSetting).filter(LangSetting.user_id==userid).one_or_none()
        if settings is not None:
            LANG[userid] = settings.lang
    return LANG[userid]
    

def translated(fn):
    #fn.__globals__['lang._lang'] = lambda x: _trans(x, 'RU')
    #fn.__globals__['_lang'] = lambda x: _trans(x, 'RU')
    #fn.__globals__['lang._tr'] = lambda x: _trans(x, 'RU')
    #fn.__globals__['_tr'] = lambda x: _trans(x, 'RU')

    def decorated_translated(message = None, *args, **kwargs):
        fn.__globals__['_tr'] = _tr #lambda x: _trans(x, lang)
        fn.__globals__['lang._tr'] = _tr #lambda x: _trans(x, lang)
        print("Lang is %s!" % globals().get('_lang'))
        return fn(message, *args, **kwargs)

    return decorated_translated

