#!/usr/bin/python3
# данный модуль при F5 позволяет запускать бота. При использовании flask ,большое внимание уделять threaded=False - многопоточность и ассихронность.  
import os
import flask
from telebot import types
from config import *
from bot import bot
import bot_handlers
import sqlalchemy
from models.Database import engine, session

server = flask.Flask(__name__)
#bot.delete_webhook()
bot.set_webhook(url="https://peterbot.rf70.ru/"+TOKEN, certificate=open('peterbot.pem'))

@server.route('/' + TOKEN, methods=['POST'])
def get_message():
    try:
        bot.process_new_updates([types.Update.de_json(flask.request.stream.read().decode("utf-8"))])
    except sqlalchemy.exc.StatementError:
        session.rollback()
    return "!", 200


@server.route('/', methods=["GET"])
def index():
    return "Bot '%s' WebHook on guard" % bot.get_me(), 200


if __name__ == "__main__":
    server.run(host="0.0.0.0", port=int(os.environ.get('PORT', 5000)), threaded=False)
