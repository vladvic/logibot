#!/usr/bin/python3
import traceback
from bot import bot
from telebot import types #Импортируем возможност использоват types.
from emoji import emojize #Импортируем эмодзи.
import re
from messages import *
from models import User
from models.Geo import City, Region, Country
from models.Database import engine, session
import Ask
import process.Request
import process.Offer
from process.Post import *
from lang import set_lang, get_translator, _tr
import lang
from monitor import send_monitored


@bot.message_handler(commands=['help'])
@lang.translated
@Ask.Session()
def send_help(message, user = None):
    chatid = message.chat.id
    bot.send_message(message.chat.id, _tr(HELP_MESSAGE), parse_mode='Markdown')


@bot.message_handler(commands=['start'])
@lang.translated
@Ask.Session()
def send_welcome(message, user = None):
    chatid = message.chat.id
    bot.send_message(message.chat.id, _tr(HELLO_MESSAGE) % message.from_user.first_name, parse_mode='Markdown')
    send_help(message)


@lang.translated
def switch_language(message, userid, lng, *args):
    # if canceled
    global _tr
    if args[0] == True:
        bot.send_message(message.chat.id, _tr(CANCELED))
        return
    set_lang(userid, lng, *args)
    _tr = get_translator(lng)
    bot.send_message(message.chat.id, _tr(LANG_CHANGED_MESSAGE) % lng)


@lang.translated
@Ask.Session(registered=True)
def comment(message, user, text, canceled):
    if canceled:
        bot.send_message(message.chat.id, _tr(CANCELED), parse_mode='Markdown')
        return
        
    admins = session.query(User.User).filter_by(is_admin=True).all()
    sender_link = "[%s](tg://user?id=%s)" % (user.first_name, user.user_id)
    msg = _tr(MESSAGE_FROM) % (sender_link, text)
    for admin in admins:
        bot.send_message(admin.user_id, msg, parse_mode='Markdown')


@bot.message_handler(commands=['comment'])
@lang.translated
@Ask.Session(registered=True)
def leave_comment(message, user = None):
    Ask.Text(message, _tr(LEAVE_COMMENT), comment)


@bot.message_handler(commands=['lang'])
@lang.translated
def language(message, user = None):
    userid = message.from_user.id
    langs = ['RU', 'EN', 'UZ', 'KZ']
    languages = [{'title': n, 'data': n} for n in langs]
    Ask.Selection(message, _tr(LANG_MESSAGE), lambda msg, *args: switch_language(msg, userid, *args), languages, 4)


@bot.message_handler(commands=['register'])
@lang.translated
@Ask.Session(registered=False)
def register(message, user = None):
    if user is not None:
        bot.send_message(message.chat.id, _tr(REG_REGISTERED))
        return
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True, one_time_keyboard=True)
    button_phone = types.KeyboardButton(text=_tr(REG_SEND_PHONE_MESSAGE), request_contact=True)
    keyboard.add(button_phone)
    bot.send_message(message.chat.id, _tr(REG_PHONE_MESSAGE), reply_markup=keyboard)
    bot.register_next_step_handler(message, end_registration)


@lang.translated
@Ask.Session(registered=False)
def end_registration(message, user = None):
    if message.text == '/cancel':
        bot.send_message(message.chat.id, _tr(CANCELED))
        return
    if message.contact is None:
        register(message)
        return

    user = User.User()
    user.first_name = message.contact.first_name
    user.last_name = message.contact.last_name
    user.phone = message.contact.phone_number
    user.user_id = message.contact.user_id
    user.username = message.from_user.username
    session.add(user)
    session.commit()
    bot.send_message(message.chat.id, _tr(REG_FINISHED))

    sender_link = "[%s](tg://user?id=%s)" % (user.first_name, user.user_id)
    monitored = _tr(MONITOR_REGISTERED) % (sender_link)
    send_monitored(monitored)


@bot.callback_query_handler(func=lambda call: (call.data.split(':')[0] == 'delete'))
def delete_post(call):
    post_id = call.data.split(':')[1]
    post = session.query(User.Post).filter_by(id=post_id).one_or_none()
    if post is not None:
        from_destination = post.from_destination
        to_destination = post.to_destination
        session.delete(post)
        session.delete(from_destination)
        session.delete(to_destination)
        session.commit()
        bot.send_message(call.message.chat.id, _tr(POST_DELETED), parse_mode='Markdown')


@bot.message_handler(commands=['list'])
@lang.translated
@Ask.Session(registered=True)
def post_list(message, user):
    posts = user.posts

    if not posts:
        bot.send_message(message.chat.id, _tr(POST_NONE), parse_mode='Markdown')

    for post in posts:
        keyboard = types.InlineKeyboardMarkup()
        button = types.InlineKeyboardButton(_tr(DELETE), callback_data="delete:%d" % post.id)
        keyboard.add(button)
        print("PROCESSING list command: %s" % post_to_message(post))
        bot.send_message(message.chat.id, post_to_message(post), reply_markup=keyboard, parse_mode='Markdown')


@bot.callback_query_handler(func=lambda call: (call.data.split(':')[0] == 'contact'))
@lang.translated
@Ask.Session(registered=True)
def contact(call, user):
    data = call.data.split(':')
    contact_id = data[1]
    username = data[2]

    receiver = session.query(User.User).filter_by(user_id=contact_id).one_or_none()

    if receiver is None:
        bot.send_message(call.message.chat.id, _tr(NO_USER)) #, parse_mode='Markdown')
        return

    receiver_link = "[%s](tg://user?id=%s)" % (receiver.first_name, receiver.user_id)
    sender_link = "[%s](tg://user?id=%s)" % (user.first_name, user.user_id)

    bot.send_message(contact_id, _tr(CONTACT) % sender_link, parse_mode='Markdown')
    bot.send_message(call.message.chat.id, _tr(SELF_CONTACT) % receiver_link, parse_mode='Markdown')

    match = User.Match()
    match.user1_id = contact_id
    match.user2_id = call.message.chat.id
    session.add(match)
    session.commit()


@bot.message_handler(commands=['monitor'])
@lang.translated
@Ask.Session(registered=True)
def monitor(message, user):
    if not user.is_admin:
        bot.send_message(message.chat.id, _tr(NOT_ADMIN), parse_mode='Markdown')
        return

    monitor = session.query(User.Monitor).filter_by(user_id=user.user_id).one_or_none()

    if monitor is None:
        monitor = User.Monitor()
        monitor.user_id = user.user_id
        session.add(monitor)
        status = 1
    else:
        session.delete(monitor)
        status = 0
    session.commit()
    bot.send_message(message.chat.id, _tr(MONITOR_STATUS) % status, parse_mode='Markdown')


@bot.message_handler(commands=['findall'])
@lang.translated
@Ask.Session(registered=True)
def post_find(message, user):
    posts = user.posts

    if not posts:
        bot.send_message(message.chat.id, _tr(POST_NONE), parse_mode='Markdown')

    for post in posts:
        counters = find_messages(post)
        for counter in counters:
            keyboard = types.InlineKeyboardMarkup()
            button = types.InlineKeyboardButton(_tr(SEND_CONTACT), callback_data="contact:%s:%s" % (counter.user.user_id, counter.user.username))
            keyboard.add(button)
            print("PROCESSING findall command: %s" % post_to_message(counter))
            bot.send_message(message.chat.id, post_to_message(counter), reply_markup=keyboard, parse_mode='Markdown')


if __name__ == '__main__':
    proceed = True
    while proceed:
        try:
            bot.polling()
            proceed = False
        except sqlalchemy.exc.StatementError:
            session.rollback()
        except Exception as e:
            tb = traceback.format_exc()
            print(tb)
            pass

