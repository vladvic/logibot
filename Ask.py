import datetime
from messages import * # Инмпортируем все с файла сообщений
from emoji import emojize
from telebot import types
from bot import bot # Импортируем объект бота
from models.Database import engine, session
from models.Geo import City, Region, Country
from models.User import User
import re
import lang


def Session(registered=None):
    def decorator(fn):
        def decorated(message, *args, **kwargs):
            if not hasattr(message, 'chat'):
                userid = message.message.chat.id
            else:
                userid = message.chat.id
            user = session.query(User).filter(User.user_id==userid).one_or_none()
            if registered is not None:
                if registered and user is None:
                    bot.send_message(userid, _tr(REG_NOT_REGISTERED))
                    return
                elif not registered and user is not None:
                    bot.send_message(userid, _tr(REG_REGISTERED))
                    return
            fn.__globals__['_tr'] = globals().get('_tr')
            return fn(message, user, *args, **kwargs)

        return decorated
    return decorator


class Sequence:

    def __init__(self, message, text, sequence, finish_callback):
        self._telegram_id = message.chat.id
        self._finish = finish_callback
        self._sequence = sequence
        self._answers = []
        self._next = 0

        bot.send_message(self._telegram_id, text, parse_mode= 'Markdown')
        self.ask(message)


    def get_answer(self, message, answer, cancel):
        if cancel:
            self._finish(message, self._answers, cancel)
            return
        self._next += 1
        self._answers.append(answer)
        if len(self._sequence) <= self._next:
            self._finish(message, self._answers, cancel)
            return
        self.ask(message)


    def ask(self, message):
        nxt = self._sequence[self._next]
        args = nxt.get('args', [])
        nxt['ask'](message, nxt['message'], lambda msg, ans, cancel: self.get_answer(msg, ans, cancel), *args)


class Number:

    def __init__(self, message, text, finish_callback):
        self._telegram_id = message.chat.id
        self._text = text
        self._finish = finish_callback
        self.ask(message)


    def ask(self, message):
        self._message = bot.send_message(self._telegram_id, self._text, parse_mode= 'Markdown')
        bot.register_next_step_handler(message, lambda message: self.get_answer(message))


    def get_answer(self, message):
        if message.text == '/cancel':
            self._finish(message, None, True)
            del self
            return
        self._answer = float(re.sub(r'[^0-9.]', '', message.text))
        bot.send_message(self._telegram_id, self._answer)
        bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
        self._finish(message, self._answer, False)
        del self


class Text:

    def __init__(self, message, text, finish_callback):
        self._telegram_id = message.chat.id
        self._text = text
        self._finish = finish_callback
        self.ask(message)


    def ask(self, message):
        self._message = bot.send_message(self._telegram_id, self._text, parse_mode= 'Markdown')
        bot.register_next_step_handler(message, lambda message: self.get_answer(message))


    def get_answer(self, message):
        if message.text == '/cancel':
            self._finish(message, None, True)
            del self
            return
        self._answer = message.text
        bot.send_message(self._telegram_id, self._answer)
        bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
        self._finish(message, self._answer, False)
        del self


class Selection:
    _id = 0

    def __init__(self, message, text, finish_callback, objects, columns=2, any_data=None, pad=False, replace_title=False):
        self._telegram_id = message.chat.id
        self._text = text
        self._objects = objects
        self._columns = columns
        self._any_data = any_data
        self._finish = finish_callback
        self._pad = pad
        self._id = Selection._id
        self._replace_title = replace_title
        Selection._id += 1
        self._last_messages = []
        self._selection = None
        self.ask(message)


    def finish(self, message, canceled=False):
        if self._selection is None:
            self._finish(message, self._any_data, canceled)
            return
        self._finish(message, self._objects[self._selection]['data'], canceled)
        del self


    def ask(self, message):
        if message.text == '/cancel':
            self.finish(message, True)
            return

        keyboard = []
        my_id = 'select%s' % self._id

        if self._any_data is not None:
            keyboard.append([{'title': _tr(ANY), 'data': '%s:%s' % (my_id, self._any_data)}])

        i = 0
        keys = []
        for obj in self._objects:
            if len(keys) == self._columns:
                keyboard.append(keys)
                keys = []
            keys.append({'title': obj['title'], 'data': ('%s:%s' % (my_id, i))})
            i += 1

        while self._pad and (len(keys) < self._columns) and (keys):
            keys.append({'title': ' ', 'data': 'pad'})

        keyboard.append(keys)
        self.send_objects(self._text, keyboard)
        bot.register_next_step_handler(message, lambda message: self.ask(message))

        @bot.callback_query_handler(func=lambda call: (call.data.split(':')[0] == my_id))
        def process_selection(call):
            proceed = False
            for msg in self._last_messages:
                if msg.message_id == call.message.message_id:
                    proceed = True
                    break

            if not proceed:
                return

            bot.clear_step_handler_by_chat_id(chat_id=call.message.chat.id)

            if call.data[(len(my_id)+1):] == self._any_data:
                self.replace_message(_tr(ANY))
            else:
                self._selection = int(call.data[len(my_id)+1:])
                self.replace_message(self._objects[self._selection]['title'])
            self.finish(call.message)

        @bot.callback_query_handler(func=lambda call: (call.data[0:3] == "pad"))
        def process_pad(call):
            return

    def replace_message(self, text):
        if not self._last_messages:
            return

        messages = self._last_messages
        if self._replace_title:
            bot.edit_message_text(text, chat_id=messages[0].chat.id, message_id=messages[0].message_id)
        else:
            bot.edit_message_text(self._text, chat_id=messages[0].chat.id, message_id=messages[0].message_id, parse_mode='Markdown')
            bot.send_message(self._telegram_id, text, parse_mode='Markdown')

        for msg in messages[1:]:
            bot.delete_message(chat_id=msg.chat.id, message_id=msg.message_id)

        self._last_messages = []


    def send_objects(self, text, objects):
        text += emojize('\n\* :right_arrow:')
        i = 0
        keyboard = types.InlineKeyboardMarkup()
        self._last_messages = []
        for row in objects:
            keys = []
            for obj in row:
                button = types.InlineKeyboardButton(obj['title'], callback_data=obj['data'])
                keys.append(button)

            if i > 30:
                self._last_messages.append(bot.send_message(self._telegram_id, text, reply_markup=keyboard, parse_mode= 'Markdown'))
                text = emojize('\* :right_arrow:')
                keyboard = types.InlineKeyboardMarkup()
                i = 0

            keyboard.row(*keys)
            i += 1
        self._last_messages.append(bot.send_message(self._telegram_id, text, reply_markup=keyboard, parse_mode= 'Markdown'))



class Date:
    _days = [31, 29, 31, 30, 31, 30, 31, 30, 31, 31, 30, 31]


    def __init__(self, message, text, finishedCallback):
        self._telegram_id = message.chat.id
        self._callback = finishedCallback
        self._date = 1
        self._month = 0
        self._year = None
        self._done = False
        self._text = text
        self.start(message)


    def start(self, message):
        bot.send_message(self._telegram_id, self._text, parse_mode= 'Markdown')
        self.get_year(message)


    def set_year(self, selection, msg, cancel):
        if cancel:
            self.finish(msg, True)
            return
        self._year = selection
        self.get_month(msg)


    def get_year(self, message):
        now = datetime.datetime.now()
        year = now.year

        Selection(message, _tr(YEAR_MESSAGE), lambda msg, selection, *args: self.set_year(selection, msg, *args), 
                  [{'title': year, 'data': year}, {'title': year + 1, 'data': year + 1}], 2, replace_title=True)


    def set_month(self, selection, msg, cancel):
        if cancel:
            self.finish(msg, True)
            return
        if selection != 'any':
            self._month = int(selection)
            self.get_date(msg)
        else:
            self.finish(msg)


    def get_month(self, message):
        months = []

        i = 0
        for mon in MONTH:
            months.append({'title': _tr(mon), 'data': i})
            i += 1

        Selection(message, _tr(MONTH_MESSAGE), lambda msg, selection, *args: self.set_month(selection, msg, *args), 
                  months, 3, 0, True, replace_title=True)


    def set_date(self, selection, msg, cancel):
        if cancel:
            self.finish(msg, True)
            return
        if selection != 'any':
            self._date = selection
        self.finish(msg)


    def get_date(self, message):
        days = 31
        if self._month is not None:
            days = self._days[self._month]
        i = 0
        dates = []
        for day in range(1, days + 1):
            dates.append({'title': day, 'data': day})

        Selection(message, _tr(DATE_MESSAGE), lambda msg, selection, *args: self.set_date(selection, msg, *args), 
                  dates, 7, 1, True, replace_title=True)

    def finish(self, message, canceled=False):
        self._callback(message, self, canceled)


    def date(self):
        return self._date


    def month(self):
        return self._month + 1


    def year(self):
        return self._year


    def to_date(self):
        return datetime.datetime(self._year, self._month + 1, self._date)


    def telegram_id(self):
        return self._telegram_id


    def __repr__(self):
        return "<Date(Day=%s, Month=%s, Year=%s)>" % (self._date, self._month + 1, self._year)



class Geography:
    _countries = list(session.query(Country).order_by(Country.name))


    def __init__(self, message, text, finishedCallback):
        self._telegram_id = message.chat.id
        self._callback = finishedCallback
        self._country = None
        self._region = None
        self._city = None
        self._done = False
        self._last_messages = {}
        self._text = text
        self.start(message)


    def start(self, message):
        bot.send_message(self._telegram_id, self._text, parse_mode= 'Markdown')
        self.get_country(message)


    def set_country(self, selection, msg, cancel):
        if cancel:
            self.finish(msg, True)
            return
        if selection != 'any':
            self._country = session.query(Country).filter(Country.id==int(selection)).one_or_none()
            self.get_region(msg)
        else:
            self.finish(msg)


    def get_country(self, message):
        if message.text == '/cancel':
            self.finish(message, True)
            return
        keyboard = []#types.InlineKeyboardMarkup()

        for country in self._countries:
            keyboard.append({'title': country.name, 'data': country.id})

        Selection(message, _tr(COUNTRY_MESSAGE), lambda msg, selection, *args: self.set_country(selection, msg, *args), 
                  keyboard, 1, 'any', False, replace_title=True)


    def set_region(self, selection, msg, cancel):
        if cancel:
            self.finish(msg, True)
            return
        if selection != 'any':
            self._region = session.query(Region).filter(Region.id==int(selection)).one_or_none()
            #self.get_city(msg)
            self.finish(msg)
        else:
            self.finish(msg)


    def get_region(self, message):
        regions = self._country.regions #session.query(Region).filter(Region.country_id==self._country.id).order_by(Region.name).all()

        keys = []
        for region in regions:
            button = {'title': region.name, 'data': region.id}
            keys.append(button)

        Selection(message, _tr(REGION_MESSAGE), lambda msg, selection, *args: self.set_region(selection, msg, *args), 
                  keys, 2, 'any', True, replace_title=True)


    def set_city(self, selection, msg, cancel):
        if cancel:
            self.finish(msg, True)
            return
        if selection != 'any':
            self._city = session.query(City).filter(City.id==int(selection)).one_or_none()

        self.finish(msg)
    

    def get_city(self, message):
        cities = self._region.cities #session.query(City).filter(City.region_id==self._region.id).order_by(City.name).all()

        keys = []
        for city in cities:
            button = {'title': city.name, 'data': city.id}
            keys.append(button)

        Selection(message, _tr(CITY_MESSAGE), lambda msg, selection, *args: self.set_city(selection, msg, *args), 
                  keys, 3, 'any', True, replace_title=True)


    def finish(self, message, canceled=False):
        self._done = True
        self._callback(message, self, canceled)


    def country(self):
        return self._country


    def region(self):
        return self._region


    def city(self):
        return self._city


    def telegram_id(self):
        return self._telegram_id

    def __repr__(self):
        return "<Geography(Country=%s, Region=%s, City=%s)>" % (self._country, self._region, self._city)

