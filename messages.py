# данный модуль позволяет хранить текстовые сообщения чатбота в хормате простых строк.
#from messages import *
import emoji
from emoji import emojize

ANY = "Any"
DATE_MESSAGE = "Select date"
MONTH_MESSAGE = "Select month"
YEAR_MESSAGE = "Select year"
MONTH = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

COUNTRY_MESSAGE = "Select country"
REGION_MESSAGE = "Select region"
CITY_MESSAGE = "Select city"

LANG_MESSAGE = "Select language"
LANG_CHANGED_MESSAGE = "Your language is switched to %s"

HELLO_MESSAGE = "Hi %s! This is your *Auto-Logistic-Bot* for helping you to organize your cargo movement! It can help you to speed up finding your customer. You don't have to keep posting and monitor your or others' posts in a group or community to find cargo or shipment. *Auto-Logistic-Bot* will do that automatically for you! We just make your life simpler!! Place your post using our step by step process and wait for response!!!! Your customer will find and contact you!!!! We wish you good luck!",
HELP_MESSAGE = "Enter */register* to register, */lang* to choose your language, */request* to find a shipment for your cargo, */offer* to offer your shipping services, */list* to list your posts, */cancel* to cancel current operation"

MONITOR_STATUS = "*Monitor status:* %s"
NOT_ADMIN = "You are not an administrator!"
MONITOR_REGISTERED = "New user: %s"
MONITOR_POST = "*New post from* %s:\n\n%s"

SEND_CONTACT = "Contact user"
CONTACT = "User %s wants you to contact him"
SELF_CONTACT = "You can contact %s"
NO_USER = "User does not exist!"
LEAVE_COMMENT = "*Leave comment below:*"
MESSAGE_FROM = "*Message from user *%s*:*\n%s"

REG_PHONE_MESSAGE = "Press the button to send your contact details and register"
REG_SEND_PHONE_MESSAGE = "Send contact details"
REG_NOT_REGISTERED = "You're not registered!"
REG_REGISTERED = "You're already registered!"
REG_FINISHED = "Thank you for registering!"

POST_ADDED = "*Your post is added, thank you, now wait for the response!*"
POST_DELETED = "*The post has been deleted*"
POST_TEXT = "*Enter post text*"
POST_NONE = "You have no posts!"
LOCATION_FORMAT = "    *Country:* %s\n    *Region:* %s\n    *City:* %s"
REQUEST_FORMAT = ("*Request*:\n*Origin:* \n%s\n*Destination:* \n%s\n*Ready from:* %s\n" + 
                  "*Due till:* %s\n*Cargo type:* %s\n*Weight:* %s ton\n*Volume:* %s m3" + 
                  "\n*Price:* %s%s\n*Payment type:* %s\n*Payment term:* %s\n*Post:*\n*%s*")
OFFER_FORMAT = ("*Offer*:\n*Origin:* \n%s\n*Destination:* \n%s\n*Ready from:* %s\n" + 
                  "*Due till:* %s\n*Truck type:* %s\n*Weight:* %s ton\n*Volume:* %s m3" + 
                  "\n*Price:* %s%s\n*Payment type:* %s\n*Payment term:* %s\n*Post:*\n*%s*")
TRUCK_TYPE_MESSAGE = "*Select truck type*"
CARGO_TYPE_MESSAGE = "*Select cargo type*"
PAYMENT_TYPE_MESSAGE = "*Select payment type*"
PAYMENT_TERMS_MESSAGE = "*Select payment terms*"

R_POST_MESSAGE = "*Let's add your request now. Please follow the instructions below*"
R_LOAD_MESSAGE = "*Enter approximate weight (tons)*"
R_VOLUME_MESSAGE = "*Enter approximate volume (m3)*"
R_PRICE_MESSAGE = "*Enter approximate price*"
R_FROM_MESSAGE = "*Where are you going from?*"
R_TO_MESSAGE = "*Where are you going to?*"
R_FROM_DATE_MESSAGE = "*Which date are you available from?*"
R_TO_DATE_MESSAGE = "*Which date are you available to?*"

O_POST_MESSAGE = "*Let's add your offer now. Please follow the instructions below*"
O_LOAD_MESSAGE = "*Enter approximate weight (tons)*"
O_VOLUME_MESSAGE = "*Enter approximate volume (m3)*"
O_PRICE_MESSAGE = "*Enter approximate price*"
O_FROM_MESSAGE = "*Where are you going from?*"
O_TO_MESSAGE = "*Where are you going to?*"
O_FROM_DATE_MESSAGE = "*Which date are you available from?*"
O_TO_DATE_MESSAGE = "*Which date are you available to?*"

CANCELED = "Canceled!"
DELETE = "Delete"

