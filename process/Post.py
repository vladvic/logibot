from datetime import datetime, timedelta
from sqlalchemy.orm import aliased
from sqlalchemy import or_, and_
from models import User
from models import Geo
from models.User import Post, Destination
from models.Database import engine, session
from messages import *
from bot import bot
import Ask
import lang


@lang.translated
def location_to_message(location):
    global _tr
    city = _tr(ANY)
    region = _tr(ANY)
    country = _tr(ANY)
    if location.city is not None:
        city = _tr(location.city.name)
    if location.region is not None:
        region = _tr(location.region.name)
    if location.country is not None:
        country = _tr(location.country.name)
    location = _tr(LOCATION_FORMAT) % (country, region, city)
    return location


def escape_text(txt):
    return txt.replace('*', '\*').replace('_', '\_')


@lang.translated
def request_to_message(post, from_location, to_location):
    global _tr
    currency = '' if post.currency is None else post.currency
    format_str = _tr(REQUEST_FORMAT) % (from_location, to_location, post.from_date.strftime('%Y-%m-%d'), post.to_date.strftime('%Y-%m-%d'),
                    _tr(post.cargo_type()), post.weight, post.volume, post.price,
                    currency, _tr(post.payment_type()), _tr(post.payment_term()), escape_text(post.text))
    return format_str


@lang.translated
def offer_to_message(post, from_location, to_location):
    global _tr
    currency = '' if post.currency is None else post.currency
    format_str = _tr(OFFER_FORMAT) % (from_location, to_location, post.from_date.strftime('%Y-%m-%d'), post.to_date.strftime('%Y-%m-%d'),
                    _tr(post.truck_type()), post.weight, post.volume, post.price,
                    currency, _tr(post.payment_type()), _tr(post.payment_term()), escape_text(post.text))
    return format_str


@lang.translated
def post_to_message(post):
    global _tr
    from_location = location_to_message(post.from_destination)
    to_location = location_to_message(post.to_destination)
    if post.post_type == 0:
        return request_to_message(post, from_location, to_location)
    return offer_to_message(post, from_location, to_location)


def find_post(post, post_type):
    from_destination = aliased(User.Destination)
    to_destination = aliased(User.Destination)
    today = datetime.today()
    posts = session.query(Post).join(
        from_destination, Post.from_destination_id==from_destination.id
    ).join(
        to_destination, Post.to_destination_id==to_destination.id
    ).filter(
        Post.post_type==post_type,
        or_(from_destination.country_id==post.from_destination.country_id, from_destination.country_id==None, post.from_destination.country_id==None),
        or_(from_destination.region_id==post.from_destination.region_id, from_destination.region_id==None, post.from_destination.region_id==None),
        or_(from_destination.city_id==post.from_destination.city_id, from_destination.city_id==None, post.from_destination.city_id==None),
        or_(to_destination.country_id==post.to_destination.country_id, to_destination.country_id==None, post.from_destination.country_id==None),
        or_(to_destination.region_id==post.to_destination.region_id, to_destination.region_id==None, post.from_destination.region_id==None),
        Post.from_date<=post.to_date, 
        Post.to_date>=post.from_date,
        Post.to_date>=today,
        Post.user_id!=post.user_id,
    )
    return posts


def find_requests(post):
    return find_post(post, 0).all()
    #.filter(Post.weight<=post.weight, Post.volume<=post.volume).all()


def find_offers(post):
    return find_post(post, 1).all()
    #.filter(Post.weight>=post.weight, Post.volume>=post.volume).all()


@lang.translated
def find_messages(post):
    global _tr
    if post.post_type == 0:
        return find_offers(post)
    return find_requests(post)

