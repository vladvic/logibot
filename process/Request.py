from datetime import datetime
from models import User
from models import Geo
from models.User import Post, Destination
from models.Database import engine, session
from messages import *
from telebot import types #Импортируем возможност использоват types.
from bot import bot
from process.Post import *
import Ask
import lang
from monitor import send_monitored


@lang.translated
@Ask.Session(registered=True)
def end_request(message, user, post, canceled):
    if canceled:
        bot.send_message(message.chat.id, _tr(CANCELED))
        return

    new_post = post.to_post()
    new_post.post_type = 0
    session.add(new_post)
    session.commit()

    keyboard = types.InlineKeyboardMarkup()
    button = types.InlineKeyboardButton(_tr(DELETE), callback_data="delete:%d" % new_post.id)
    keyboard.add(button)
    bot.send_message(message.chat.id, post_to_message(new_post), reply_markup=keyboard, parse_mode='Markdown')

    bot.send_message(message.chat.id, _tr(POST_ADDED), parse_mode='Markdown')
    print(post)
    offers = find_offers(new_post)
    for offer in offers:
        keyboard = types.InlineKeyboardMarkup()
        button = types.InlineKeyboardButton(_tr(SEND_CONTACT), callback_data="contact:%s:%s" % (offer.user.user_id, offer.user.username))
        keyboard.add(button)
        #print("PROCESSING request command: %s" % post_to_message(offer))
        bot.send_message(message.chat.id, post_to_message(offer), reply_markup=keyboard, parse_mode='Markdown')
        keyboard = types.InlineKeyboardMarkup()
        button = types.InlineKeyboardButton(_tr(SEND_CONTACT), callback_data="contact:%s:%s" % (user.user_id, user.username))
        keyboard.add(button)
        bot.send_message(offer.user.user_id, post_to_message(new_post), reply_markup=keyboard, parse_mode='Markdown')

    sender_link = "[%s](tg://user?id=%s)" % (user.first_name, user.user_id)
    monitored = _tr(MONITOR_POST) % (sender_link, post_to_message(new_post))
    send_monitored(monitored)


@bot.message_handler(commands=['request'])
@lang.translated
@Ask.Session(registered=True)
def get_request(message, user):
    Request(message, user, end_request)


class Destination:
    def __init__(self, dest=None):
        self.country = None
        self.region = None
        self.city = None
        if dest:
            self.country = dest.country()
            self.region = dest.region()
            self.city = dest.city()


class Request:
    
    def __init__(self, message, user, finish_callback):
        self._telegram_id = message.chat.id
        self._finish = finish_callback
        self._from = Destination()
        self._to = Destination()
        self._from_date = None
        self._to_date = None
        self._truck_type_no = None
        self._cargo_type_no = None
        self._weight = None
        self._volume = None
        self._price = None
        self._currency = None
        self._payment_type_no = None
        self._payment_term_no = None
        self._text = None
        self._user = user

        self.ask(message)


    def finish(self, message, answers, cancel):
        if cancel:
            self._finish(message, None, True)
            return
        self._from = Destination(answers[0])
        self._to = Destination(answers[1])
        self._from_date = answers[2].to_date()
        self._to_date = answers[3].to_date()
        self._cargo_type = int(answers[4])
        self._weight = int(answers[5])
        self._volume = int(answers[6])
        self._price = answers[7]
        self._payment_type = int(answers[8])
        self._payment_terms = int(answers[9])
        self._text = answers[10]
        self._finish(message, self, False)
        #print(answers)


    def ask(self, message):
        truck_types = [{'title': _tr(n), 'data': i} for (i, n) in enumerate(User.Post.truck_types)]
        #truck_types = ['Refrigirator', 'Box', 'Tanker', 'Trailer', 'Flatbed', 'Dump']
        cargo_types = [{'title': _tr(n), 'data': i} for (i, n) in enumerate(User.Post.cargo_types)]
        #cargo_types = ['Liquid', 'Loose', 'Solid', 'Palette', 'Bulky']
        payment_types = [{'title': _tr(n), 'data': i} for (i, n) in enumerate(User.Post.payment_types)]
        #payment_types = ['Cash', 'Transfer']
        payment_terms = [{'title': _tr(n), 'data': i} for (i, n) in enumerate(User.Post.payment_terms)]
        #payment_terms = ['Advance', 'Partial advance', 'Post payment']
        sequence = [
            {'message': _tr(R_FROM_MESSAGE), 'ask': Ask.Geography},
            {'message': _tr(R_TO_MESSAGE), 'ask': Ask.Geography},
            {'message': _tr(R_FROM_DATE_MESSAGE), 'ask': Ask.Date},
            {'message': _tr(R_TO_DATE_MESSAGE), 'ask': Ask.Date},
            {'message': _tr(CARGO_TYPE_MESSAGE), 'ask': Ask.Selection, 'args': [cargo_types, 1]},
            {'message': _tr(R_LOAD_MESSAGE), 'ask': Ask.Number},
            {'message': _tr(R_VOLUME_MESSAGE), 'ask': Ask.Number},
            {'message': _tr(R_PRICE_MESSAGE), 'ask': Ask.Text},
            {'message': _tr(PAYMENT_TYPE_MESSAGE), 'ask': Ask.Selection, 'args': [payment_types, 1]},
            {'message': _tr(PAYMENT_TERMS_MESSAGE), 'ask': Ask.Selection, 'args': [payment_terms, 1]},
            {'message': _tr(POST_TEXT), 'ask': Ask.Text},
        ]
        Ask.Sequence(message, _tr(R_POST_MESSAGE), sequence, lambda *args: self.finish(*args))

    def to_post(self):

        post = User.Post()

        post.user = self._user

        post.from_destination = User.Destination()
        post.from_destination.country = self._from.country
        post.from_destination.region = self._from.region
        post.from_destination.city = self._from.city
        post.to_destination = User.Destination()
        post.to_destination.country = self._to.country
        post.to_destination.region = self._to.region
        post.to_destination.city = self._to.city

        post.from_date = self._from_date
        post.to_date = self._to_date

        post.truck_type_no = 0
        post.cargo_type_no = self._cargo_type
        post.weight = int(self._weight)
        post.volume = int(self._volume)
        post.price = self._price
        post.payment_type_no = int(self._payment_type)
        post.payment_term_no = int(self._payment_terms)
        post.text = self._text

        return post


    def from_location(self):
        return self._from

    def to_location(self):
        return self._to

    def from_date(self):
        return self._from_date

    def to_date(self):
        return self._to_date

    def cargo_type(self):
        return self._cargo_type

    def truck_type(self):
        return self._truck_type

    def weight(self):
        return self._weight

    def volume(self):
        return self._volume

    def price(self):
        return self._price

    def payment_type(self):
        return self._payment_type

    def payment_terms(self):
        return self._payment_terms

    def text(self):
        return self._text

