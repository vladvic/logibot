import traceback
from bot import bot
from telebot import types #Импортируем возможност использоват types.
from emoji import emojize #Импортируем эмодзи.
import re
from messages import *
from models import User
from models.Geo import City, Region, Country
from models.Database import engine, session
import Ask
from process.Post import *
from lang import set_lang, get_translator, _tr
import lang


def send_monitored(mon):
    monitors = session.query(User.Monitor).all()

    for monitor in monitors:
        bot.send_message(monitor.user_id, mon, parse_mode='Markdown')

