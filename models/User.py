from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Boolean, DateTime, Integer, Text, String, ForeignKey, Float, BigInteger, Sequence, MetaData
from sqlalchemy.orm import relationship, object_mapper
from models.Geo import Country, Region, City
from models.Meta import Base

class Match(Base):
    __tablename__ = 'match'
    id = Column(Integer, Sequence('match_id_seq'), primary_key=True)
    user1_id = Column(String(128))
    user2_id = Column(String(128))

class Monitor(Base):
    __tablename__ = 'monitor'
    id = Column(Integer, Sequence('monitor_id_seq'), primary_key=True)
    user_id = Column(String(128))


class LangSetting(Base):
    __tablename__ = 'lang'
    id = Column(Integer, Sequence('lang_setting_id_seq'), primary_key=True)
    user_id = Column(String(128))
    lang = Column(String(8))

class User(Base):
    __tablename__ = 'user'

    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    first_name = Column(String(128))
    last_name = Column(String(128))
    phone = Column(String(128))
    user_id = Column(String(128))
    username = Column(String(128))
    is_admin = Column(Boolean, default=False)

    def __repr__(self):
        return "<User(first_name='%s', last_name='%s', phone='%s')>" % (self.first_name, self.last_name, self.phone)

class Destination(Base):
    __tablename__ = "destination"

    id = Column(Integer, Sequence('destination_id_seq'), primary_key=True)
    country_id = Column(Integer, ForeignKey('country.id'))
    country = relationship("Country", backref="destinations")
    region_id = Column(Integer, ForeignKey('region.id'))
    region = relationship("Region", backref="destinations")
    city_id = Column(Integer, ForeignKey('city.id'))
    city = relationship("City", backref="cities")

    def __repr__(self):
        return "<Destination(country=%s, region=%s, city=%s)>" % (self.country, self.region, self.city)

class Post(Base):
    __tablename__ = 'post'
    truck_types = ['Refrigirator', 'Box', 'Tanker', 'Tent', 'Trailer', 'Semi-trailer', 'Flatbed', 'Dump']
    cargo_types = ['Liquid', 'Loose', 'Solid', 'Palette', 'Bulky']
    payment_types = ['Cash', 'Transfer']
    payment_terms = ['Advance', 'Partial advance', 'Post payment']
    post_types = ['Request', 'Offer']

    id = Column(Integer, Sequence('post_id_seq'), primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    post_type = Column(Integer)
    from_destination_id = Column(Integer, ForeignKey('destination.id'))
    from_destination = relationship("Destination", foreign_keys=[from_destination_id])
    to_destination_id = Column(Integer, ForeignKey('destination.id'))
    to_destination = relationship("Destination", foreign_keys=[to_destination_id])
    from_date = Column(DateTime)
    to_date = Column(DateTime)
    truck_type_no = Column(Integer)
    cargo_type_no = Column(Integer)
    weight = Column(Float)
    volume = Column(Float)
    price = Column(String(128))
    currency = Column(String(128))
    payment_type_no = Column(Integer)
    payment_term_no = Column(Integer)
    text = Column(Text)

    user = relationship("User", backref="posts")

    def truck_type(self):
        if self.truck_type_no is None:
            return None
        return Post.truck_types[self.truck_type_no];

    def cargo_type(self):
        if self.cargo_type_no is None:
            return None
        return Post.cargo_types[self.cargo_type_no];

    def payment_type(self):
        if self.payment_type_no is None:
            return None
        return Post.payment_types[self.payment_type_no];

    def payment_term(self):
        if self.payment_term_no is None:
            return None
        return Post.payment_terms[self.payment_term_no];

    def __repr__(self):
        return ("<Post(user=%s, from_destination=%s, to_destination=%s, from_date=%s, to_date=%s, truck_type=%s, " + 
                "cargo_type=%s, weight=%s, volume=%s, price=%s, currency=%s, payment_type=%s, payment_terms=%s)>") % (
                    self.user, self.from_destination, self.to_destination, self.from_date, self.to_date, 
                    self.truck_type(), self.cargo_type(), self.weight, self.volume, self.price, 
                    self.currency, self.payment_type(), self.payment_term())


