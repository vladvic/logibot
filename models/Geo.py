from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Float, BigInteger, Sequence, MetaData
from sqlalchemy.orm import relationship, object_mapper
from models.Meta import Base

class City(Base):
    __tablename__ = 'city'

    id = Column(Integer, Sequence('city_id_seq'), primary_key=True)
    name = Column(String(128))
    region_id = Column(Integer, ForeignKey('region.id'))

    def __repr__(self):
        return "<City(name='%s')>" % self.name

class Region(Base):
    __tablename__ = 'region'

    id = Column(Integer, Sequence('region_id_seq'), primary_key=True)
    name = Column(String(128))
    country_id = Column(Integer, ForeignKey('country.id'))
    cities = relationship("City", backref="region")

    def __repr__(self):
        return "<Region(name='%s')>" % self.name

class Country(Base):
    __tablename__ = 'country'

    id = Column(Integer, Sequence('country_id_seq'), primary_key=True)
    name = Column(String(128))
    code = Column(String(8))
    regions = relationship("Region", backref="country")

    def __repr__(self):
        return "<Country(name='%s', code='%s')>" % (self.name, self.code)

